CFLAGS=-Wall -Wextra -pedantic -std=c99

all: question1 question2 triangle

question1: warning.o

question2: circle.o main.o
	$(CC) -o question2 circle.o main.o

triangle: triangle.o
	$(CC) -o triangle triangle.o

clean:
	rm circle.o main.o warning.o question2 triangle

.PHONY: clean
