///
/// File: triangle.c
///
/// program prints multiple triangles "right-aligned" triangles of this form:
///     *
///   ***
/// *****
///
/// @author mxc4400: Martin Charles
///
// // // // // // // // // // // // // // // // // // // // // // // //

#include <stdio.h>

static int ceil_div(int top, int bottom) {
  return (top + bottom - 1) / 2;
}

///
/// Function: drawTriangle
///
/// Description: draw a right-aligned triangle whose height/width is "size"
///
/// @param size  the size of the triangle to draw
static void drawTriangle(int size) {
  if (size <= 0) {
    // Can't print this. C doesn't have exceptions.
    return;
  }

  if (size % 2 == 0) {
    // If the function's argument is even, the function increments the size by 1.
    size++;
  }

  // rows_count is the number of rows the triangle will have.
  int rows_count = ceil_div(size, 2);
  int max_width = size;

  for (int row = 0; row < rows_count; row++) {
    int row_width = row * 2 + 1;

    for (int column = max_width - 1; column >= 0; column--) {
      if (column >= row_width) {
        printf(" ");
      } else {
        printf("*");
      }
    }

    printf("\n");
  }

  printf("\n");
}

/// Expected Output:
///
/// *
///
///     *
///   ***
/// *****
///
///       *
///     ***
///   *****
/// *******
///
/// 1. 1x1
/// 2. 3x5
/// 3. 4x7

#define LENGTH 3
///
/// Function: main
///
/// Description: draw 3 triangles of size 1, 4, and 6.
/// There is one blank line between each triangle, and after the last one.
///
/// @returns errorCode  error Code; EXIT_SUCCESS if no error
int main() {
  int sizes[] = {1, 4, 6};

  for (int i = 0; i < LENGTH; i++) {
    int size = sizes[i];
    drawTriangle(size);
  }

  return 0;
}
